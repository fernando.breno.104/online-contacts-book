# Online Contacts Book

Aplicação feita seguindo o tutorial da HMTMCSE.

## Repositório do projeto no GitHub

[https://github.com/hmtmcse-com/grails-tutorial-contacts-book](https://github.com/hmtmcse-com/grails-tutorial-contacts-book)

### Tag v1.0 criada ao final do tutorial

### Tag v1.01 criada após exercicios propostos no fim do tutorial
